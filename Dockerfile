
#FROM nginx:1.23.2-alpine
#COPY ./nginx.conf /etc/nginx/nginx.conf
#COPY ./dist/apps/leac /usr/share/nginx/html

### STAGE 1: Build ###
FROM node:19-alpine AS build
WORKDIR /usr/src/app
COPY ./package.json ./package-lock.json ./
COPY ./decorate-angular-cli.js ./
RUN npm install
RUN npm install -g @nrwl/cli
COPY . .
RUN npx nx build leac

### STAGE 2: Run ###
FROM nginx:1.23.2-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/apps/leac /usr/share/nginx/html
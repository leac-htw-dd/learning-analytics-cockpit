module.exports = {
  theme: {
    colors: {
      primary: {
        light: '#aad0f2',
        DEFAULT: '#72B0EA',
        dark: '#507ba4',
        hover: '#8ec0ee',
      },
      secondary: {
        light: '#E0E0E0',
        DEFAULT: '#C1C1C1',
        dark: '#040a10',
      },
      tertiary: {
        light: '#f0f4f9',
        DEFAULT: '#e6ecf5',
        dark: '#cfd4dd',
      },
      shade: {
        dark: '#323130',
        DEFAULT: '#4C4A4A',
        light: '#676464',
      },
      warning: {
        light: '#f3a0a0',
        hover: '#d92020',
        dark: '#960000',
        DEFAULT: '#FF4242',
      },
      status: {
        success: '#82F5BE',
        failed: '#F5B982',
        repeat: '#f9d5b4',
        neutral: '#D9D9D9',
      },
      exam: {
        sp: '#7fc8f8',
        mp: '#D2F1E4',
        apl: '#FFE45E',
        pvl: '#FBCAEF',
      },
      events: {
        lecture: '#5b8dbb',
        exercise: '#446a8c',
        practice: '#2e465e',
      },
      grey: {
        light: '#F7F7F7',
        DEFAULT: '#cccccc',
      },
      white: '#ffffff',
      black: '#000000',
    },
    fontFamily: {
      'sans': ['Open Sans'],
      body: ['Open Sans', 'sans-serif'],
      display: ['Open Sans', 'sans-serif'],
    },
    extend: {
      transitionProperty: {
        height: 'height',
        width: 'width',
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
};

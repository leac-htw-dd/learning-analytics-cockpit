
import { IIdentified } from "./identified";
import { IProfessor, IStudent, IUser } from "./IUser";
import { ExamRequirementType, SemesterType, StudyNiveau, CourseType } from "./Types";

export interface ISubjectCondition extends IIdentified {
    nr: string;
    name: string;
    description: string;
    faculty: string;
    professor: IProfessor | any;
    niveau: StudyNiveau[];
}


export type StudyNiveau = "BACHELOR" | "DIPLOM" | "MASTER";
export type StudyMode = "FULL" // TBD;


export type SemesterType = 'WS' | 'SS';

export type CoursEventType = 'LECTURE' | 'EXERCICE' | 'PRACTICE';
export type CourseType = 'MODULE' | 'THESIS' | 'INTERNSHIP';
export type ExamRequirementType = 'SP' | 'MP' | 'APL' | 'PVL' | 'B' | 'V' // TBD;

export type ExamResultStatus = 'REGISTERED' | 'NOT_PASSED' | 'PASSED' | 'FINAL_NOT_PASSED';

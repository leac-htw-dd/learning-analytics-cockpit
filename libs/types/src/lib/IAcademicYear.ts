import { IIdentified } from './identified';

export interface IAcadamicYear extends IIdentified {
  description: string;
  winterSemester: ISemester;
  summerSemester: ISemester;
}

export interface ISemester {
  lecturePeriod: IPeriod;
  examinationPeriod: IPeriod;
  lectureFreePeriod: IPeriod;
  reRegistrationPeriod: IPeriod;
  lectureFreeDays: (IPeriod | Date)[];
}

export interface IPeriod {
  from: Date;
  to: Date;
}

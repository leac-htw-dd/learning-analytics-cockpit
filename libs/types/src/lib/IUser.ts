import { IIdentified } from "./identified";
import { IStudy } from "./IStudy";
import { ICourseResult } from "./ICourse";
import { StudyNiveau } from "./Types";

export interface IUser extends IIdentified {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export interface IStudent extends IIdentified, IUser {
    study: IStudy | any;
    degree: StudyNiveau;
    enrollmentYear: number;
    courses: ICourseResult[];
    averageStudyGrade: number;
}

export interface IProfessor extends IUser {
    title: string;
}

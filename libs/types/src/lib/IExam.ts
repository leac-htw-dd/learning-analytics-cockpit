import { ICourseCondition } from "./ICourse";
import { IIdentified } from "./identified";
import { IExamResult } from "./IExamResult";
import { IProfessor } from "./IUser";
import { ExamRequirementType } from "./Types";

export interface IExamCondition extends IIdentified {
    type: ExamRequirementType;
    defaultSemester: number;
    isMandatory: boolean;
    weighting: number;
    durationInMinutes?: number;
    courseCondition: ICourseCondition | any;
}

export interface IExam extends IIdentified {
    nr?: string;
    description?: string;
    date: Date;
    location: string;
    examCondition: IExamCondition | any;
    result?: IExamResult | any;
    professor: IProfessor | any;
    registeredParticipants: number;
}

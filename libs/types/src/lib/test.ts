// TODO remove
// test uml diagramm generation

export interface IAcadamicYear extends IIdentified {
    winterSemester: ISemester;
    summerSemester: ISemester;
}

export interface IIdentified {
    _id?: any;
}

export interface ISubjectCondition extends IIdentified {
    nr: string;
    name: string;
    description: string;
    faculty: string;
    professor: IProfessor | any;
    niveau: StudyNiveau[];
}

export interface IStudy extends IIdentified {
    nr: string;
    name: string;
    faculty: string;
    degree: StudyNiveau;
    requiredCredits: number;
    regularSemesterCount: number;
    studyMode: StudyMode;
    personResponsible: IProfessor | any;
}

export interface IExamResult extends IIdentified {
    participant: IStudent | any;
    exam: IExam | any;
    requirementsMet: boolean;
    tries: number;
    credits: number;
    status: ExamResultStatus;
    grade?: number;
    expectedGrae?: number;
  }

  export interface IUser extends IIdentified {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export interface IStudent extends IIdentified, IUser {
    study: IStudy | any;
    degree: StudyNiveau;
    enrollmentYear: number;
    courses: ICourseResult[];
    averageStudyGrade: number;
}

export interface IProfessor extends IUser {
    title: string;
}


export interface IExamCondition{
    type: ExamRequirementType;
    defaultSemester: number;
    isOralExam: boolean;
    isMandatory: boolean;
    weighting: number;
    durationInMinutes?: number;
}

export interface IExam extends IIdentified, IExamCondition {
    nr: string;
    description: string;
    date: Date;
    location: string;
    subject: ISubjectCondition | any;
    professor: IProfessor | any;
    registeredParticipants: number;
}

export interface ISemester {
    lecturePeriod: IPeriod;
    examinationPeriod: IPeriod;
    lectureFreePeriod: IPeriod;
    reRegistrationPeriod: IPeriod;
    lectureFreeDays: (IPeriod | Date)[];
}

export interface IPeriod {
    from: Date;
    to: Date;
}

export interface ICourseCondition extends IIdentified {
    name: string;
    isMandatory: boolean;
    type: CourseType;
    semesterCount: number;
    turnus?: SemesterType;
    credits: number;
    workLoad: number;
    selfStudyTime?: number;
    teachingFormat?: ITeachingFormat;
    subject: ISubjectCondition | any;
    exams: IExamCondition[];
}

export interface ICourse extends IIdentified {
    description: string;
    year: number;
    courseCondition: ICourseCondition | any;
    registeredParticipantCount: number;
    registeredParticipants: IStudent[] | any[];
    teachingMaterial: ITeachingMaterial;
    contactEmail: string;
    tutor?: IUser | any;
}

export interface ITeachingMaterial {
    lectureUrls?: string[];
    exerciseUrls?: string[];
    assignmentUrls?: string[];
}

export interface ITeachingFormat {
    defaultSemester: number;
    lecturesPerWeek: number;
    exercisesPerWeek: number;
    practicesPerWeek: number;
}

export interface ICourseResult {
    course: ICourse | any;
    results: IExamResult[];
    totalExamCount: number;
    passed: boolean;
}

export interface ModifiedGradeResult {
    averageStudyGrade: number;
    subjects: ICourseResult[];
}


export type StudyNiveau = "BACHELOR" | "DIPLOM" | "MASTER";
export type StudyMode = "FULL" // TBD;


export type SemesterType = 'WS' | 'SS';

export type CourseType = 'MODULE' | 'THESIS' | 'INTERNSHIP';
export type ExamRequirementType = 'TEST' | "ASSIGNMENT" // TBD;

export type ExamResultStatus = 'REGISTERED' | 'NOT_PASSED' | 'PASSED' | 'FINAL_NOT_PASSED';

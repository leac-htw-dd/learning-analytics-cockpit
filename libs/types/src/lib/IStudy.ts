import { IIdentified } from "./identified";
import { IProfessor } from "./IUser";
import { StudyMode, StudyNiveau } from "./Types";

export interface IStudy extends IIdentified {
    nr: string;
    name: string;
    faculty: string;
    degree: StudyNiveau;
    achievedCredits: number;
    failedCredits: number;
    requiredCredits: number;
    grade: number;
    regularSemesterCount: number;
    studyMode: StudyMode;
    personResponsible: IProfessor | any;
}

import { IIdentified } from "./identified";
import { IExamCondition } from "./IExam";
import { IExamResult } from "./IExamResult";
import { ISubjectCondition } from "./ISubject";
import { IStudent, IUser } from "./IUser";
import { CourseType, CoursEventType, SemesterType } from "./Types";

export interface ICourseCondition extends IIdentified {
    name: string;
    isMandatory: boolean;
    type: CourseType;
    semesterCount: number;
    turnus?: SemesterType;
    credits: number;
    workLoad: number;
    selfStudyTime?: number;
    teachingFormat?: ITeachingFormat;
    subject: ISubjectCondition | any;
    exams: IExamCondition[] | any[];
}

export interface ICourse extends IIdentified {
    description: string;
    year: string;
    courseCondition: ICourseCondition | any;
    registeredParticipantCount: number;
    registeredParticipants: IStudent[] | any[];
    teachingMaterial: ITeachingMaterial;
    contactEmail: string;
    isReexamination?: boolean;
    tutor?: IUser | any;
    examResult?: IExamResult;
}

export interface ICourseEvent {
  name: string;
  course: ICourse | any;
  type: CoursEventType;
  time: {
    start: string;
    end: string;
  };
}

export interface ITeachingMaterial {
    lectureUrls?: string[];
    exerciseUrls?: string[];
    assignmentUrls?: string[];
}

export interface ITeachingFormat {
    defaultSemester: number;
    lecturesPerWeek: number;
    exercisesPerWeek: number;
    practicesPerWeek: number;
}

export interface ICourseResult {
    course: ICourse | any;
    results: IExamResult[];
    totalExamCount: number;
    passed: boolean;
}

export interface ModifiedGradeResult {
    averageStudyGrade: number;
    subjects: ICourseResult[];
}

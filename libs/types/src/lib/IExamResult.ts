import { IIdentified } from './identified';
import { IExam } from './IExam';
import { IStudent } from './IUser';
import { ExamResultStatus } from './Types';

export interface IExamResult extends IIdentified {
  participant: IStudent | any;
  exam: IExam | any;
  requirementsMet: boolean;
  tries: number;
  credits: number;
  status: ExamResultStatus;
  grade?: number;
  expectedGrade?: number;
}

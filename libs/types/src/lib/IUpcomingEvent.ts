import { ICourse } from "./ICourse";
import { IExam } from "./IExam";
import { ExamRequirementType } from "./Types";

export interface IUpcomingEvent {
  date: Date;
  description: string;
  course: ICourse | any;
  exam: IExam | any;
  type: ExamRequirementType;
}

// Types and General
export * from './lib/Types';

// Interfaces
export * from './lib/IAcademicYear';
export * from './lib/ICourse';
export * from './lib/identified';
export * from './lib/IExam';
export * from './lib/IExamResult';
export * from './lib/IStudy';
export * from './lib/ISubject';
export * from './lib/IUser';
export * from './lib/IUpcomingEvent';

import { Injectable } from '@angular/core';
import { IStudent } from '@learning-analytics-cockpit/types';
import { CurrentUser } from './data/students';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  getCurrentUser(): IStudent {
    return CurrentUser;
  }
}

import { Injectable } from '@angular/core';
import { IUpcomingEvent } from '@learning-analytics-cockpit/types';
import { UpcomingEvents } from './data/upcoming-events';

@Injectable({
  providedIn: 'root',
})
export class UpcomingEventsService {
  constructor() {}

  getAll(): IUpcomingEvent[] {
    return UpcomingEvents;
  }
}

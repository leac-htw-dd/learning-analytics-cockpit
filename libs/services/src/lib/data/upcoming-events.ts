import { IUpcomingEvent } from '@learning-analytics-cockpit/types';
import { addDays } from 'date-fns';

export const UpcomingEvents: IUpcomingEvent[] = [
  {
    date: addDays(new Date(), 4),
    description: 'Prüfung SP I-126',
    type: 'SP',
    course: null,
    exam: null,
  },
  {
    date: addDays(new Date(), 2),
    description: 'Abgabe APL I-146',
    type: 'APL',
    course: null,
    exam: null,
  },
];

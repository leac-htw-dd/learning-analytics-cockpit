import { IStudent } from '@learning-analytics-cockpit/types';

export const CurrentUser: IStudent = {
  study: 'MI',
  degree: 'BACHELOR',
  enrollmentYear: 2021,
  courses: [],
  averageStudyGrade: 1.9,
  firstName: 'Vorname',
  lastName: 'Nachname',
  email: 'student@htw-dresden.de',
  password: 'htw',
};

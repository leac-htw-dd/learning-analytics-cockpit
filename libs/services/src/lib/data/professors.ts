import { IProfessor } from '@learning-analytics-cockpit/types';

export const Professors: IProfessor[] = [
  {
    _id: 'lilifeld',
    title: 'Prof.',
    firstName: 'Lili',
    lastName: 'Feld',
    email: 'Lili.Feld@htw-dresden.de',
    password: '',
  },
  {
    _id: 'kunibertotto',
    title: 'Prof.',
    firstName: 'Kunibert',
    lastName: 'Otto',
    email: 'Kunibert.Otto@htw-dresden.de',
    password: '',
  },
  {
    _id: 'arnoldbach',
    title: 'Prof.',
    firstName: 'Arnold',
    lastName: 'Bach',
    email: 'Arnold.Bach@htw-dresden.de',
    password: '',
  },
  {
    _id: 'clothildeklein',
    title: 'Prof.',
    firstName: 'Clothilde',
    lastName: 'Klein',
    email: 'Clothilde.Klein@htw-dresden.de',
    password: '',
  },
  {
    _id: 'ramonasantillan',
    title: 'Prof.',
    firstName: 'Ramona',
    lastName: 'Santillan',
    email: 'Lili.Feld@htw-dresden.de',
    password: '',
  },
  {
    _id: 'ellengoodman',
    title: 'Prof.',
    firstName: 'Ellen',
    lastName: 'Goodman',
    email: 'Ellen.Goodman@htw-dresden.de',
    password: '',
  },
];

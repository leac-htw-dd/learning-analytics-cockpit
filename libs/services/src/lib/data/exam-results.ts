/* participant: IStudent | any;
exam: IExam | any;
requirementsMet: boolean;
tries: number;
credits: number;
status: ExamResultStatus;
grade?: number;
expectedGrade?: number;

export type ExamResultStatus =
'REGISTERED' | 'NOT_PASSED' | 'PASSED' | 'FINAL_NOT_PASSED';
*/

import { IExamResult } from '@learning-analytics-cockpit/types';

export const ExamResults: IExamResult[] = [
  // 1. Semester
  {
    _id: 'examR001',
    exam: 'exam001',
    requirementsMet: true,
    tries: 1,
    credits: 3,
    status: 'PASSED',
    grade: 1.7,
    expectedGrade: 2.0,
    participant: undefined,
  },
  {
    _id: 'examR002',
    exam: 'exam002',
    requirementsMet: true,
    tries: 1,
    credits: 5,
    status: 'PASSED',
    grade: 2.3,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR003',
    exam: 'exam003',
    requirementsMet: true,
    tries: 1,
    credits: 5,
    status: 'PASSED',
    grade: 2.3,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR004',
    exam: 'exam004',
    requirementsMet: true,
    tries: 1,
    credits: 5,
    status: 'PASSED',
    grade: 4.0,
    expectedGrade: 2.0,
    participant: undefined,
  },
  {
    _id: 'examR005',
    exam: 'exam005',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'NOT_PASSED',
    grade: 5.0,
    expectedGrade: 4.0,
    participant: undefined,
  },
  // 2. Semester
  {
    _id: 'examR006',
    exam: 'exam006',
    requirementsMet: true,
    tries: 1,
    credits: 3,
    status: 'NOT_PASSED',
    grade: 5.0,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR007',
    exam: 'exam007',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'PASSED',
    grade: 2.3,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR008',
    exam: 'exam008',
    requirementsMet: true,
    tries: 1,
    credits: 5,
    status: 'PASSED',
    grade: 2.0,
    expectedGrade: 2.0,
    participant: undefined,
  },
  {
    _id: 'examR009',
    exam: 'exam009',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'NOT_PASSED',
    grade: 5.0,
    expectedGrade: 3.0,
    participant: undefined,
  },
  // 3. Semester
  {
    _id: 'examR010',
    exam: 'exam010',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'REGISTERED',
    grade: 5.0,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR011',
    exam: 'exam011',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'REGISTERED',
    grade: 2.3,
    expectedGrade: 2.0,
    participant: undefined,
  },{
    _id: 'examR012',
    exam: 'exam012',
    requirementsMet: true,
    tries: 1,
    credits: 4,
    status: 'REGISTERED',
    grade: 2.0,
    expectedGrade: 2.1,
    participant: undefined,
  },
  {
    _id: 'examR013',
    exam: 'exam013',
    requirementsMet: true,
    tries: 1,
    credits: 6,
    status: 'REGISTERED',
    grade: 5.0,
    expectedGrade: 2.3,
    participant: undefined,
  },
  {
    _id: 'examR014',
    exam: 'exam014',
    requirementsMet: true,
    tries: 2,
    credits: 4,
    status: 'REGISTERED',
    grade: 5.0,
    expectedGrade: 4.0,
    participant: undefined,
  },
];

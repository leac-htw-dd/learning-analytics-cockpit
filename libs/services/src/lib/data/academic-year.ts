import { IAcadamicYear } from '@learning-analytics-cockpit/types';

export const AcadamicYears: IAcadamicYear[] = [
  {
    description: '2021/22',
    winterSemester: {
      lecturePeriod: {
        from: new Date('10.04.2021'),
        to: new Date('01.28.2022'),
      },
      examinationPeriod: {
        from: new Date('01.30.2022'),
        to: new Date('02.18.2022'),
      },
      lectureFreePeriod: {
        from: new Date('01.30.2022'),
        to: new Date('02.28.2022'),
      },
      reRegistrationPeriod: {
        from: new Date('01.23.2022'),
        to: new Date('02.17.2022'),
      },
      lectureFreeDays: [],
    },
    summerSemester: {
      lecturePeriod: {
        from: new Date('03.20.2022'),
        to: new Date('07.08.2022'),
      },
      examinationPeriod: {
        from: new Date('07.10.2022'),
        to: new Date('07.29.2022'),
      },
      lectureFreePeriod: {
        from: new Date('07.10.2022'),
        to: new Date('08.31.2022'),
      },
      reRegistrationPeriod: {
        from: new Date('07.03.2022'),
        to: new Date('07.28.2022'),
      },
      lectureFreeDays: [],
    },
  },
  {
    description: '2022/23',
    winterSemester: {
      lecturePeriod: {
        from: new Date('04.10.2022'),
        to: new Date('01.28.2023'),
      },
      examinationPeriod: {
        from: new Date('01.30.2023'),
        to: new Date('02.18.2023'),
      },
      lectureFreePeriod: {
        from: new Date('01.30.2023'),
        to: new Date('02.28.2023'),
      },
      reRegistrationPeriod: {
        from: new Date('01.23.2023'),
        to: new Date('02.17.2023'),
      },
      lectureFreeDays: [],
    },
    summerSemester: {
      lecturePeriod: {
        from: new Date('03.20.2023'),
        to: new Date('07.08.2023'),
      },
      examinationPeriod: {
        from: new Date('07.10.2023'),
        to: new Date('07.29.2023'),
      },
      lectureFreePeriod: {
        from: new Date('07.10.2023'),
        to: new Date('08.31.2023'),
      },
      reRegistrationPeriod: {
        from: new Date('07.03.2023'),
        to: new Date('07.28.2023'),
      },
      lectureFreeDays: [],
    },
  },
  {
    description: '2023/24',
    winterSemester: {
      lecturePeriod: {
        from: new Date('04.10.2023'),
        to: new Date('01.28.2024'),
      },
      examinationPeriod: {
        from: new Date('01.30.2024'),
        to: new Date('02.18.2024'),
      },
      lectureFreePeriod: {
        from: new Date('01.30.2024'),
        to: new Date('02.28.2024'),
      },
      reRegistrationPeriod: {
        from: new Date('01.23.2024'),
        to: new Date('02.17.2024'),
      },
      lectureFreeDays: [],
    },
    summerSemester: {
      lecturePeriod: {
        from: new Date('03.20.2024'),
        to: new Date('07.08.2024'),
      },
      examinationPeriod: {
        from: new Date('07.10.2024'),
        to: new Date('07.29.2024'),
      },
      lectureFreePeriod: {
        from: new Date('07.10.2024'),
        to: new Date('08.31.2024'),
      },
      reRegistrationPeriod: {
        from: new Date('07.03.2024'),
        to: new Date('07.28.2024'),
      },
      lectureFreeDays: [],
    },
  },
];

import {
  ICourse,
  ITeachingMaterial,
} from '@learning-analytics-cockpit/types';

const teachingMaterial: ITeachingMaterial = {
  lectureUrls: ['https://bildungsportal.sachsen.de/opal/auth/'],
  exerciseUrls: ['https://bildungsportal.sachsen.de/opal/auth'],
  assignmentUrls: ['https://bildungsportal.sachsen.de/opal/auth/'],
};

function getRandomBetween(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

export const Courses: ICourse[] = [
  // 1. Semester
  {
    _id: 'course0001',
    description: 'Elektronik für Medieninformatiker 22',
    year: 'WS 21/22',
    courseCondition: 'courseC0001',
    registeredParticipantCount: 12,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Lili.Feld@htw-dresden.de',
  },
  {
    _id: 'course0002',
    description: 'Grundlagen der Informatik I 22',
    year: 'WS 21/22',
    courseCondition: 'courseC0002',
    registeredParticipantCount: 14,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0003',
    description: 'Programmierung I 22',
    year: 'WS 21/22',
    courseCondition: 'courseC0003',
    registeredParticipantCount: 12,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Arnold.Bach@htw-dresden.de',
  },
  {
    _id: 'course0004',
    description: 'Betriebssysteme I 22',
    year: 'WS 21/22',
    courseCondition: 'courseC0004',
    registeredParticipantCount: 5,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Clothilde.Klein@htw-dresden.de',
  },
  {
    _id: 'course0005',
    description: 'Algebra und höhere Mathematik 1 - 22',
    year: 'WS 21/22',
    courseCondition: 'courseC0005',
    registeredParticipantCount: 15,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Clothilde.Klein@htw-dresden.de',
  },
  // 2. Semester
  {
    _id: 'course0006',
    description: 'Algebra und höhere Mathematik 2 - 22',
    year: 'SS 21/22',
    courseCondition: 'courseC0006',
    registeredParticipantCount: 12,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Lili.Feld@htw-dresden.de',
  },
  {
    _id: 'course0007',
    description: 'Digitale Signalverarbeitung 22',
    year: 'SS 21/22',
    courseCondition: 'courseC0007',
    registeredParticipantCount: 10,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0008',
    description: 'Programmierung II 22',
    year: 'SS 21/22',
    courseCondition: 'courseC0008',
    registeredParticipantCount: 12,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Arnold.Bach@htw-dresden.de',
  },
  {
    _id: 'course0009',
    description: 'Datenbanksysteme I 22',
    year: 'SS 21/22',
    courseCondition: 'courseC0009',
    registeredParticipantCount: 25,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Clothilde.Klein@htw-dresden.de',
  },
  // 3. Semester
  {
    _id: 'course0010',
    description: 'Rechnerarchitektur  23',
    year: 'WS 22/23',
    courseCondition: 'courseC0010',
    registeredParticipantCount: 3,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Lili.Feld@htw-dresden.de',
  },
  {
    _id: 'course0011',
    description: 'Software Engineering I  23',
    year: 'WS 22/23',
    courseCondition: 'courseC0011',
    registeredParticipantCount: 13,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0012',
    description: 'Datenbanksysteme II  23',
    year: 'WS 22/23',
    courseCondition: 'courseC0012',
    registeredParticipantCount: 14,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0013',
    description: 'Computergrafik/Visualisierung I 23',
    year: 'WS 22/23',
    courseCondition: 'courseC0013',
    registeredParticipantCount: 24,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0025',
    description: 'Algebra und höhere Mathematik 1 - 23',
    year: 'WS 22/23',
    courseCondition: 'courseC0005',
    registeredParticipantCount: 12,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Clothilde.Klein@htw-dresden.de',
  },
  // 4. Semester
  {
    _id: 'course0014',
    description: 'Internet-Technologien I 23',
    year: 'SS 22/23',
    courseCondition: 'courseC0014',
    registeredParticipantCount: 17,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0015',
    description: 'Computergrafik/Visualisierung II 23',
    year: 'SS 22/23',
    courseCondition: 'courseC0015',
    registeredParticipantCount: 27,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },{
    _id: 'course0016',
    description: 'Gestaltung interaktiver Systeme 23',
    year: 'SS 22/23',
    courseCondition: 'courseC0016',
    registeredParticipantCount: 18,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0017',
    description: 'Entwicklungswerkzeuge für MM-Systeme 23',
    year: 'SS 22/23',
    courseCondition: 'courseC0017',
    registeredParticipantCount: 20,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0018',
    description: 'Medienproduktion 23',
    year: 'SS 22/23',
    courseCondition: 'courseC0018',
    registeredParticipantCount: 23,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  // 5. Semester
  {
    _id: 'course0019',
    description: 'Internet-Technologien II 24',
    year: 'WS 23/24',
    courseCondition: 'courseC0019',
    registeredParticipantCount: 5,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0020',
    description: 'Informatikrecht 24',
    year: 'WS 23/24',
    courseCondition: 'courseC0020',
    registeredParticipantCount: 15,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0021',
    description: 'Beleuchtung und Rendering 24',
    year: 'WS 23/24',
    courseCondition: 'courseC0021',
    registeredParticipantCount: 19,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0022',
    description: 'Computeranimation 24',
    year: 'WS 23/24',
    courseCondition: 'courseC0022',
    registeredParticipantCount: 9,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  // 6. Semester
  {
    _id: 'course0023',
    description: 'Praxisprojekt 24',
    year: 'SS 23/24',
    courseCondition: 'courseC0023',
    registeredParticipantCount: 0,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
  {
    _id: 'course0024',
    description: 'Bachelorarbeit 24',
    year: 'SS 23/24',
    courseCondition: 'courseC0024',
    registeredParticipantCount: 0,
    registeredParticipants: [],
    teachingMaterial: teachingMaterial,
    contactEmail: 'Kunibert.Otto@htw-dresden.de',
  },
];

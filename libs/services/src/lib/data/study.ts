import { IStudy } from "@learning-analytics-cockpit/types";

export const Study: IStudy = {
  _id: 'MI',
  nr: "I43",
  name: "Medieninformatik",
  faculty: "Informatik/Mathematik",
  degree: "BACHELOR",
  achievedCredits: 0,
  failedCredits: 0,
  requiredCredits: 150,
  regularSemesterCount: 6,
  grade: 0,
  studyMode: "FULL",
  personResponsible: 'lilifeld'
}

import { Injectable } from '@angular/core';
import {
  ICourse,
  IExamResult,
  IPeriod,
  IProfessor,
  ISemester,
  IStudent,
  IStudy,
} from '@learning-analytics-cockpit/types';
import { getYear, isWithinInterval } from 'date-fns';
import { AcadamicYears } from './data/academic-year';
import { CourseConditions } from './data/course-conditions';
import { Courses } from './data/courses';
import { ExamConditions } from './data/exam-conditions';
import { ExamResults } from './data/exam-results';
import { Exams } from './data/exams';
import { Professors } from './data/professors';
import { CurrentUser } from './data/students';
import { Study } from './data/study';
import { SubjectConditions } from './data/subject-conditions';

export interface ISemesterData extends ISemester {
  name: string;
  grade?: number;
  failedCredits: number;
  achievedCredits: number;
  credits: number;
  workLoad: number;
  expectedGrade?: number;
  courses: ICourse[];
  exams: IExamResult[];
}

export interface IStudyData {
  data: IStudy;
  period: IPeriod;
  semester: ISemesterData[];
}

export interface DashboardData {
  student: IStudent;
  study: IStudyData;
}

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  getProfessors(): IProfessor[] {
    return Professors;
  }

  getDashboardData() {
    const academicYears = AcadamicYears;

    const dashboardData: DashboardData = {
      student: CurrentUser,
      study: {
        data: Study,
        period: {
          from: academicYears[0].winterSemester.lecturePeriod.from,
          to: academicYears[2].summerSemester.lectureFreePeriod.to,
        },
        semester: [],
      },
    };

    const examConditions = ExamConditions;
    const exams = Exams;
    const examResults = ExamResults;

    exams.map((exam) => {
      exam.examCondition = examConditions.find(
        (examCondition) => examCondition._id === exam.examCondition
      );
    });

    examResults.map((examResult) => {
      examResult.exam = exams.find((exam) => exam._id === examResult.exam);
    });

    const courseConditions = CourseConditions;
    courseConditions.map((courseCondition) => {
      const subjectCondition = SubjectConditions.find(
        (subjectCondition) => courseCondition.subject === subjectCondition._id
      );

      courseCondition.subject = subjectCondition;

      return courseCondition;
    });

    const courses = Courses;
    courses.map((course) => {
      const courseCondition = CourseConditions.find(
        (courseCondition) => course.courseCondition === courseCondition._id
      );

      course.courseCondition = courseCondition;

      let courseResult = examResults.find((result) => {
        return (
          result.exam.examCondition.courseCondition ===
          course.courseCondition._id
        );
      });

      if (!courseResult) {
        return course;
      }

      const courseYear = +`20${course.year.slice(6, 8)}`;
      const examYear = getYear(courseResult.exam.date);

      if (courseYear !== examYear) {
        courseResult = examResults.find(
          (result) =>
            result.exam._id !== courseResult?.exam._id &&
            result.exam.examCondition._id ===
              courseResult?.exam.examCondition._id
        );
      }

      course.examResult = courseResult;

      return course;
    });

    const semesters: ISemesterData[] = [];

    academicYears.forEach((year) => {
      semesters.push({
        name: `WS ${year.description.slice(2, 7)}`,
        ...year.winterSemester,
        courses: [],
        exams: [],
        achievedCredits: 0,
        failedCredits: 0,
        credits: 0,
        workLoad: 0,
      });
      semesters.push({
        name: `SS ${year.description.slice(2, 7)}`,
        ...year.summerSemester,
        courses: [],
        exams: [],
        achievedCredits: 0,
        failedCredits: 0,
        credits: 0,
        workLoad: 0,
      });
    });

    let studyGrade = 0;

    semesters.map((semester, index) => {
      const semesterCourses = courses.filter(
        (course) => course.year === semester.name
      );

      if (!semesterCourses || semesterCourses.length === 0) {
        return;
      }

      semester.workLoad = semesterCourses
        .map((course) => {
          return (
            course.courseCondition.workLoad +
            course.courseCondition.selfStudyTime
          );
        })
        .reduce((a, b) => a + b);

      const semesterExams = examResults.filter((examResult) => {
        return isWithinInterval(examResult.exam.date, {
          start: semester.examinationPeriod.from,
          end: semester.examinationPeriod.to,
        });
      });

      semester.exams = semesterExams;

      if (semesterExams.length && semesterExams.length > 0) {
        let passedCount = 0;
        const grade = semesterExams
          .map((exam) => {
            if (exam.status === 'PASSED') {
              passedCount++;
              return exam.grade ?? 0;
            }

            return 0;
          })
          .reduce((a, b) => a + b);

        semester.grade =
          passedCount > 0 ? +(grade / passedCount).toFixed(1) : undefined;

        semester.achievedCredits = semesterExams
          .map((exam) => {
            if (exam.status === 'PASSED') {
              return exam.credits;
            }

            return 0;
          })
          .reduce((a, b) => a + b);

        semester.failedCredits = semesterExams
          .map((exam) => {
            if (exam.status === 'NOT_PASSED') {
              return exam.credits;
            }

            return 0;
          })
          .reduce((a, b) => a + b);
      }

      semester.credits = semesterCourses
        .map((course) => course.courseCondition.credits)
        .reduce((a, b) => a + b);
      semester.courses = semesterCourses;

      studyGrade += semester.grade ?? 0;
      dashboardData.study.data.achievedCredits += semester.achievedCredits;
      dashboardData.study.data.failedCredits += semester.failedCredits;
    });

    if (studyGrade !== 0) {
      const gradedSemester = semesters.filter((semester) => semester.grade);
      studyGrade = +(studyGrade / gradedSemester.length).toFixed(1);
    }

    dashboardData.study.data.grade = studyGrade;
    dashboardData.study.semester = semesters;

    return dashboardData;
  }
}

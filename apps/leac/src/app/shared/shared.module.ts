import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { UpcomingEventsComponent } from './components/upcoming-events/upcoming-events.component';
import { CalendarDailyComponent } from './components/calendar-daily/calendar-daily.component';
import { ServicesModule } from '@learning-analytics-cockpit/services';
import { IconCardComponent } from './components/icon-card/icon-card.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { UtilModule } from '../util/util.module';
import { LayoutComponent } from './components/layout/layout.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    UpcomingEventsComponent,
    CalendarDailyComponent,
    IconCardComponent,
    ProgressBarComponent,
    LayoutComponent
  ],
  imports: [CommonModule, RouterModule, ServicesModule, UtilModule],
  exports: [
    HeaderComponent,
    SidebarComponent,
    UpcomingEventsComponent,
    CalendarDailyComponent,
    IconCardComponent,
    ProgressBarComponent,LayoutComponent
  ],
})
export class SharedModule {}

import { Component, Input, OnInit } from '@angular/core';
import { smoothWidth } from '../../../util/animation';

@Component({
  selector: 'learning-analytics-cockpit-progress-bar',
  animations: [smoothWidth],
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
})
export class ProgressBarComponent implements OnInit {
  @Input() total!: number;
  @Input() success!: number;
  @Input() failed!: number;

  isAnimating = true;
  offset = 6;
  totalWidth = 0;
  successWidth = 0;
  failedWidth = 0;
  successWidthInPx = '0px';
  failedWidthInPx = '0px';

  ngOnInit(): void {
    const totalWidth = document.getElementById('pb_total')?.clientWidth;

    if (!totalWidth) {
      return;
    }

    this.totalWidth = totalWidth;

    const successElement = document.getElementById('pb_success');
    const failedElement = document.getElementById('pb_failed');

    const successPercentage = this.success / this.total;
    const failedPercentage = this.failed / this.total;

    setTimeout(() => {
      this.successWidth = successPercentage * totalWidth;
      this.successWidthInPx = `${successPercentage * totalWidth}px`;
      setTimeout(() => {
        this.failedWidth = failedPercentage * totalWidth;
        this.failedWidthInPx = `${failedPercentage * totalWidth}px`;
        setTimeout(() => {
          this.isAnimating = false;
        }, 100);
      }, 500);
    }, 500);
  }

  getTotalX() {
    return `${this.totalWidth - this.offset * 3}px`;
  }

  getFailedX() {
    return `${this.successWidth + this.failedWidth - this.offset}px`;
  }

  getSuccessX() {
    return `${this.successWidth - this.offset}px`;
  }
}

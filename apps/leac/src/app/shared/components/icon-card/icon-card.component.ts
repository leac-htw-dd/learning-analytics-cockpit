import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'learning-analytics-cockpit-icon-card',
  animations: [
    trigger('appearCard', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('400ms', style({ opacity: 0 }))
      ])
    ])
  ],
  templateUrl: './icon-card.component.html',
  styleUrls: ['./icon-card.component.scss']
})
export class IconCardComponent {
  @Input() value!: string | number;
  @Input() isIcon = true;
  @Input() symbol!: string;
  @Input() text!: string;
  @Input() size: 'SMALL' | 'MEDIUM' = 'SMALL'
}

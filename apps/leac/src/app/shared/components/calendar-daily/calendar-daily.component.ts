import { Component } from '@angular/core';
import {
  CoursEventType,
  ICourseEvent,
} from '@learning-analytics-cockpit/types';
import { addDays, subDays } from 'date-fns';

export interface HourSlot {
  name: string;
  time: number;
}

interface IConvertedCourseEvent {
  name: string;
  type: CoursEventType;
  time: {
    start: string;
    startNumber: number;
    end: string;
    endNumber: number;
  };
  concurrentEvents?: number;
}

interface IEventSlot {
  event: ICourseEvent | IConvertedCourseEvent;
  start: boolean;
  end: boolean;
}

@Component({
  selector: 'learning-analytics-cockpit-calendar-daily',
  templateUrl: './calendar-daily.component.html',
  styleUrls: ['./calendar-daily.component.scss'],
})
export class CalendarDailyComponent {
  date: Date = new Date();
  hours: HourSlot[];

  startTime = 6;
  startHour = '6:00';
  hoursCount = 16;

  data: ICourseEvent[] = [
    {
      name: 'Software Engineering I - Übung',
      course: '',
      type: 'EXERCICE',
      time: {
        start: '10:00',
        end: '11:30',
      },
    },
    {
      name: 'Datenbanksysteme II - Vorlesung',
      course: '',
      type: 'LECTURE',
      time: {
        start: '10:30',
        end: '12:00',
      },
    },
    {
      name: 'Computergrafik/Visualisierung - Praktikum',
      course: '',
      type: 'PRACTICE',
      time: {
        start: '11:45',
        end: '13:15',
      },
    },
    /* {
      name: 'Test',
      color: '#b33f62',
      time: {
        start: '15:00',
        end: '16:30',
      }
    } */
  ];

  convertedData: IConvertedCourseEvent[] | undefined;

  dataDisplay: {
    slot: HourSlot;
    events: (IEventSlot | null)[];
  }[] = [];

  constructor() {
    this.hours = this.createHourTimeList();
    this.convertedData = this.data
      .map((element) => {
        const converted = element as unknown as IConvertedCourseEvent;
        const startTimeSplit = element.time.start.split(':');
        converted.time.startNumber =
          Number.parseInt(startTimeSplit[0]) +
          (Number.parseInt(startTimeSplit[1]) / 15) * 0.25;
        const endTimeSplit = element.time.end.split(':');
        converted.time.endNumber =
          Number.parseInt(endTimeSplit[0]) +
          (Number.parseInt(endTimeSplit[1]) / 15) * 0.25;

        return converted;
      })
      .sort(
        (a: IConvertedCourseEvent, b: IConvertedCourseEvent) =>
          a.time.startNumber - b.time.startNumber
      );

    const timeBox: {
      events: IConvertedCourseEvent[];
      start?: number;
      end?: number;
    } = {
      events: [],
    };
    this.convertedData.forEach((element) => {
      if (timeBox.events.length === 0) {
        timeBox.events.push(element);
        timeBox.start = element.time.startNumber;
        timeBox.end = element.time.endNumber;
      } else {
        if (timeBox.start && timeBox.end) {
          if (
            (element.time.startNumber >= timeBox.start &&
              element.time.startNumber < timeBox.end) ||
            (element.time.startNumber < timeBox.start &&
              element.time.endNumber > timeBox.end) ||
            (element.time.endNumber > timeBox.start &&
              element.time.endNumber <= timeBox.end)
          ) {
            timeBox.events.push(element);
            if (element.time.endNumber > timeBox.end) {
              timeBox.end = element.time.endNumber;
            }
            timeBox.events.forEach((element) => {
              if (this.convertedData) {
                const index = this.convertedData?.findIndex(
                  (eventData) => eventData.name === element.name
                );
                this.convertedData[index].concurrentEvents =
                  timeBox.events.length;
              }
            });
          } else {
            timeBox.events = [];
            timeBox.start = undefined;
            timeBox.end = undefined;
          }
        }
      }
    });
    this.createArray();
  }

  changeDay(next: boolean) {
    this.date = next ? addDays(this.date, 1) : subDays(this.date, 1);
  }

  createArray() {
    if (this.convertedData) {
      this.convertedData.forEach((event: IConvertedCourseEvent) => {
        let eventIndex: number;
        let eventIndexSet = false;
        this.hours.forEach((hours: HourSlot, index) => {
          let elementIndex = this.dataDisplay.findIndex(
            (element) => element.slot.time === hours.time
          );
          let addEvent: IEventSlot | null = null;
          if (elementIndex >= 0) {
            this.dataDisplay[elementIndex].events.push(addEvent);
          } else {
            this.dataDisplay.push({
              slot: hours,
              events: [addEvent],
            });
            elementIndex = this.dataDisplay.length - 1;
          }

          const slotStart = this.startTime + index * 0.25;
          const slotEnd = slotStart + 0.25;

          if (
            (event.time.startNumber >= slotStart &&
              event.time.startNumber < slotEnd) ||
            (event.time.startNumber < slotStart &&
              event.time.endNumber > slotEnd) ||
            (event.time.endNumber > slotStart &&
              event.time.endNumber <= slotEnd)
          ) {
            addEvent = {
              event: event,
              start: false,
              end: false,
            };
            addEvent.start = event.time.startNumber >= slotStart;
            addEvent.end = event.time.endNumber <= slotEnd;
            if (elementIndex >= 0 && !eventIndexSet) {
              eventIndex = this.dataDisplay[elementIndex].events.findIndex(
                (element) => element === null
              );
              eventIndexSet = true;
            }

            this.dataDisplay[elementIndex].events[eventIndex] = addEvent;
          }
        });
      });
      let most = 0;
      this.dataDisplay.forEach((element) => {
        const count = element.events.filter((event) => event !== null).length;
        most = count > most ? count : most;
      });
      this.dataDisplay = this.dataDisplay.map((element) => {
        element.events = element.events.slice(0, most);
        return element;
      });
    }
  }

  createHourTimeList(): HourSlot[] {
    let startTime = this.startTime;
    let startHour = this.startHour;
    return Array.from(Array(this.hoursCount * 4).keys()).map((_, index) => {
      const timeslot = {
        name: '',
        time: 0,
      };

      if (index === 0) {
        timeslot.time = startTime;
        timeslot.name = startHour;
        return timeslot;
      }

      startTime = +(startTime + 0.25).toFixed(2);
      timeslot.time = +startTime.toFixed(2);

      startHour = `${startHour.split(':')[0]}:${+startHour.split(':')[1] + 15}`;

      if (+startHour.split(':')[1] === 60) {
        startHour = `${+startHour.split(':')[0] + 1}:00`;
      }

      timeslot.name = startHour;

      return timeslot;
    });
  }
}

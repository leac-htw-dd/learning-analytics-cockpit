import { Component, OnInit } from '@angular/core';
import { UpcomingEventsService } from '@learning-analytics-cockpit/services';
import { IUpcomingEvent } from '@learning-analytics-cockpit/types';

@Component({
  selector: 'learning-analytics-cockpit-upcoming-events',
  templateUrl: './upcoming-events.component.html',
  styleUrls: ['./upcoming-events.component.scss']
})
export class UpcomingEventsComponent implements OnInit  {
  upcomingEvents: IUpcomingEvent[] = [];

  constructor(private upcomingEventService: UpcomingEventsService) {}

  ngOnInit(): void {
    this.upcomingEvents = this.upcomingEventService.getAll();
  }

}

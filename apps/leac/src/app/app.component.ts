import { Component } from '@angular/core';

@Component({
  selector: 'learning-analytics-cockpit-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'leac';
}

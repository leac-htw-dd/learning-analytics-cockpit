import {
  animate,
  animateChild,
  group,
  query,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import {
  DashboardData,
  DashboardService,
  ISemesterData,
  IStudyData,
} from '@learning-analytics-cockpit/services';
import { IStudent } from '@learning-analytics-cockpit/types';
import * as d3 from 'd3';
import { smoothWidth } from './../util/animation';

export type ZoomSteps = 'STUDY' | 'SEMESTER' | 'COURSE';

@Component({
  selector: 'learning-analytics-cockpit-dashboard',
  animations: [
    /* smoothWidth, */
    trigger('size', [
      state(
        'zero',
        style({ width: '0', display: 'hidden', overflow: 'hidden' })
      ),
      state('small', style({ width: '8.333333%', height: '64px' })),
      state('big', style({ width: '100%' })),
      transition(
        'zero <=> small',
        group([
          query('.tester', [animateChild(), animate('1s ease')], {
            optional: true,
          }),
          animate('450ms'),
        ])
      ),
      transition(
        'small <=> big',
        group([
          animate('450ms'),
          query('.tester', [animateChild(), animate('1s ease')], {
            optional: true,
          }),
        ])
      ),
    ]),
  ],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, AfterViewInit {
  currentUser: IStudent | undefined;
  study: IStudyData | undefined;

  step: ZoomSteps = 'STUDY';
  selectedSemester: number | null = null;
  selectedCourse = '';
  isLecture = true;

  totalworkLoad = 0;

  constructor(
    private element: ElementRef,
    private dashboardService: DashboardService
  ) {}

  @HostListener('wheel', ['$event'])
  public onScroll(event: WheelEvent) {
    const targetElement = event.target as HTMLElement;

    if (!targetElement) {
      return;
    }

    /* const scale = `scale(${1.6})`;
    const origin = `0% 0%`; */

    /* targetElement.style["transform"] = scale;
    targetElement.style["transformOrigin"] = origin;

    this.element.nativeElement.scrollLeft += event.deltaY; */
  }

  ngOnInit(): void {
    const data = this.dashboardService.getDashboardData();
    // console.log(data);
    this.currentUser = data.student;
    this.study = data.study;

    this.totalworkLoad = this.study?.semester
      .map((semester) => semester.workLoad)
      .reduce((a, b) => a + b);
  }

  selectStudy() {
    this.step = 'STUDY';
    this.selectedSemester = null;

    setTimeout(() => {
      this.calculateWidthOfSemester();
    }, 50);
  }

  selectSemester(semesterName: string) {
    let semesterIndex = 0;

    this.study?.semester.find((semester, index) => {
      if (semester.name === semesterName) {
        semesterIndex = index;
      }
      return semester.name === semesterName;
    });

    this.selectedSemester = semesterIndex;
    this.step = 'SEMESTER';
  }

  selectCourse(courseId: string) {
    this.selectedCourse = courseId;
    this.step = 'COURSE';
  }

  changeLecturePerion(isLecture: boolean) {
    this.isLecture = isLecture;
  }

  ngAfterViewInit(): void {
    const svg: d3.Selection<Element, unknown, HTMLElement, unknown> =
      d3.select('#zoom');
    const zoomFn: d3.ZoomBehavior<Element, unknown> = d3
      .zoom()
      .on('zoom', function (event) {
        svg.attr('transform', event.transform);
      });
    svg.call(zoomFn);
    this.calculateWidthOfSemester();
  }

  /* handleZoom(event: any) {
    const element = d3.select('zoom').attr('transform', event.transform);
  } */

  getSize(index: number): string {
    if (this.selectedSemester !== null && this.step === 'SEMESTER') {
      if (
        index === this.selectedSemester + 1 ||
        this.selectedSemester - 1 === index
      ) {
        return 'small';
      }
      if (this.selectedSemester === index) {
        return 'big';
      }
      return 'zero';
    } else if (this.selectedSemester !== null && this.step === 'COURSE') {
      if (this.selectedSemester === index) {
        return 'big';
      }
      return 'zero';
    } else {
      return '';
    }
  }

  calculateWidthOfSemester() {
    this.study?.semester.map((semester, index) => {
      const element = document.getElementById(semester.name);
      if (!element) {
        return;
      }

      element.style.width = this.getWidth(index);
    });
  }

  getWidth(semesterIndex: number): string {
    if (this.step !== 'STUDY') {
      return 'auto';
    }

    if (!this.totalworkLoad || !this.study?.semester[semesterIndex].workLoad) {
      return '100%';
    }

    const percent = (
      (this.study?.semester[semesterIndex].workLoad / this.totalworkLoad) *
      100
    ).toFixed(0);

    return `${percent}%`;
  }
}

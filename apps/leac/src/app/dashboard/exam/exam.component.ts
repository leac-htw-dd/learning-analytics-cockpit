import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { ISemesterData } from '@learning-analytics-cockpit/services';
import { IExamResult } from '@learning-analytics-cockpit/types';
import { differenceInDays } from 'date-fns';
import { smoothWidth } from '../../util/animation';

@Component({
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  // host: { '(@switchTimeslot.done)': 'setPositionOfExam()' },
  animations: [
    smoothWidth,
    trigger('appear', [
      state(
        'in',
        style({
          'max-height': '500px',
          opacity: '1',
          visibility: 'visible',
        })
      ),
      state(
        'out',
        style({
          'max-height': '0px',
          opacity: '0',
          visibility: 'hidden',
        })
      ),
      transition('out => in', [animate('.4s ease')]),
      transition('in => out', [animate('.4s ease')]),
    ]),
  ],
  selector: 'learning-analytics-cockpit-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss'],
})
export class ExamComponent implements AfterViewInit, OnChanges {
  @Input() semester!: ISemesterData;
  @Input() exam!: IExamResult;
  @Input() selectedCourse = '';
  @Input() isLecture!: boolean;

  daysBetweenPeriod = 0;
  daysBetweenExam = 0;
  currentWidth = 0;

  isLoading = true;

  ngAfterViewInit(): void {
    this.daysBetweenPeriod = differenceInDays(
      this.semester.examinationPeriod.to,
      this.semester.examinationPeriod.from
    );
    this.daysBetweenExam = differenceInDays(
      this.exam.exam.date,
      this.semester.examinationPeriod.from
    );

    this.currentWidth = 0;
    setTimeout(() => {
      this.setPositionOfExam();
    }, 500);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isLecture'] && !changes['semester']) {
      this.setPositionOfExam(true);
    }
  }

  setPositionOfExam(isLectureChange?: boolean) {
    this.isLoading = true;
    const examPeriod = document.getElementById(
      'exam_period' + this.semester.name
    );
    const exam = document.getElementById(this.exam._id);
    const space = document.getElementById(this.exam._id + 'space');

    if (!examPeriod || !exam || !space) {
      return;
    }

    let periodWidth = 0;
    if (!isLectureChange) {
      const computedStyle = getComputedStyle(examPeriod);
      periodWidth = examPeriod.clientWidth;
      periodWidth -=
        parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight) +
        (this.isLecture ? 0 : 150);
    }

    if (isLectureChange) {
      if (this.isLecture) {
        periodWidth = this.currentWidth / 4;
      }

      if (!this.isLecture) {
        periodWidth = this.currentWidth * 4;
      }
    }

    space.style.left = `${
      (periodWidth / this.daysBetweenPeriod) * this.daysBetweenExam
    }px`;
    exam.style.left = `${
      (periodWidth / this.daysBetweenPeriod) * this.daysBetweenExam
    }px`;

    this.currentWidth = periodWidth;
    this.isLoading = false;
  }
}

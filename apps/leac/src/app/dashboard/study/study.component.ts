import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input } from '@angular/core';
import { IStudyData } from '@learning-analytics-cockpit/services';
import { IStudent } from '@learning-analytics-cockpit/types';
import { ZoomSteps } from '../dashboard.component';
import { smoothHeight } from './../../util/animation';

@Component({
  selector: 'learning-analytics-cockpit-study',animations: [

    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    ),smoothHeight,
  ],
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.scss']
})
export class StudyComponent {
  @Input() step!: ZoomSteps;
  @Input() study: IStudyData | undefined;
  @Input() user: IStudent | undefined;
}

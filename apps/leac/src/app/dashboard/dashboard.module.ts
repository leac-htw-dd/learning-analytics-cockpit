import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UtilModule } from '../util/util.module';
import { ServicesModule } from '@learning-analytics-cockpit/services';
import { StudyComponent } from './study/study.component';
import { SemesterComponent } from './semester/semester.component';
import { CourseComponent } from './course/course.component';
import { TimelineComponent } from './timeline/timeline.component';
import { SharedModule } from '../shared/shared.module';
import { ExamComponent } from './exam/exam.component';

@NgModule({
  declarations: [
    DashboardComponent,
    StudyComponent,
    SemesterComponent,
    CourseComponent,
    TimelineComponent,
    ExamComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UtilModule,
    SharedModule,
    ServicesModule,
  ],
})
export class DashboardModule {}

import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  ICourse,
  IExamCondition,
  IExamResult,
  IProfessor,
} from '@learning-analytics-cockpit/types';
import { Professors } from 'libs/services/src/lib/data/professors';
import { smoothHeight } from '../../util/animation';
import { ZoomSteps } from '../dashboard.component';

interface IDocument {
  name: string;
  date: Date;
  read: boolean;
}

@Component({
  selector: 'learning-analytics-cockpit-course',
  animations: [smoothHeight],
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit {
  @Input() step!: ZoomSteps;
  @Input() course!: ICourse;
  @Input() selectedCourse = '';
  @Input() selectedSemester!: boolean;
  @Input() isLecture!: boolean;
  @Input() index = 0;

  @Output() stepSelected = new EventEmitter<ZoomSteps>();
  @Output() courseSelected = new EventEmitter<string>();

  exams: (IExamCondition & { result?: IExamResult })[] = [];

  tabSelected: 'LECTURE' | 'EXERCICE' | 'PRACTICE' = 'LECTURE';

  lectures: IDocument[] = [].constructor(7);
  exercices: IDocument[] = [].constructor(7);
  practices: IDocument[] = [].constructor(4);

  professor: IProfessor | undefined;

  ngOnInit(): void {
    this.exams = this.course.courseCondition.exams;

    // TODO add APLs etc.
    if (this.course.examResult) {
      this.exams.map((exam) => {
        if (exam.type === this.course.examResult?.exam.examCondition.type) {
          exam.result = this.course.examResult;
        }
      });

      this.exams = this.exams.sort((a, b) => (a.type === 'SP' ? -1 : 1));
    }

    // TODO make this real data
    for (let index = 0; index < 7; index++) {
      const lecture = {
        name: `vl_${this.course.courseCondition.subject.name}_0${index}.pdf`,
        date: new Date(),
        read: false,
      };
      this.lectures[index] = lecture;
    }

    for (let index = 0; index < 7; index++) {
      const exercice = {
        name: `ueb_${this.course.courseCondition.subject.name}_0${index}.pdf`,
        date: new Date(),
        read: false,
      };
      this.exercices[index] = exercice;
    }

    for (let index = 0; index < 5; index++) {
      const practice = {
        name: `prakt_${this.course.courseCondition.subject.name}_0${index}.pdf`,
        date: new Date(),
        read: false,
      };
      this.practices[index] = practice;
    }

    const professors = Professors;
    this.professor = professors.find(
      (professor) =>
        professor._id === this.course.courseCondition.subject.professor
    );
  }

  selectCourse(course: ICourse) {
    this.selectedCourse = course._id;
    this.courseSelected.emit(this.selectedCourse);
    this.stepSelected.emit('COURSE');
  }

  getGrade(): string | number {
    if (!this.course.examResult?.grade) {
      return '/';
    }
    return this.course.examResult?.status !== 'REGISTERED'
      ? this.course.examResult?.grade
      : '/';
  }

  getExpectedGrade(): string | number {
    if (!this.course.examResult?.expectedGrade) {
      return '/';
    }
    return this.course.examResult?.expectedGrade;
  }

  getGradeDiff(): string | number {
    if (!this.course.examResult?.expectedGrade) {
      return '/';
    }

    if (!this.course.examResult?.grade) {
      return '/';
    }

    if (
      this.course.examResult?.grade &&
      this.course.examResult?.status === 'REGISTERED'
    ) {
      return '/';
    }

    if (this.course.examResult?.expectedGrade > this.course.examResult?.grade) {
      return (
        this.course.examResult?.expectedGrade - this.course.examResult?.grade
      ).toFixed(1);
    }

    return (
      this.course.examResult?.grade - this.course.examResult?.expectedGrade
    ).toFixed(1);
  }

  getWidthByHours(course: ICourse): string {
    if (this.step === 'COURSE') {
      return '100%';
    }

    const maxWidth = course.courseCondition.type === 'MODULE' ? 300 : 700;
    const selfStudyTime = course.courseCondition.selfStudyTime ?? 75;
    const workLoad = course.courseCondition.workLoad ?? 75;

    return `${((selfStudyTime + workLoad) / maxWidth) * 100}%`;
  }

  getHeightByHours(course: ICourse): string {
    const maxHeight = course.courseCondition.type === 'MODULE' ? 50 : 25;
    const credits = course.courseCondition.credits ?? 0;
    const height = credits * maxHeight <= 100 ? 125 : credits * maxHeight;

    return `${height}px`;
  }
}

import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy } from '@angular/compiler';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { IStudyData } from '@learning-analytics-cockpit/services';
import { smoothHeight, smoothWidth } from '../../util/animation';
import { ZoomSteps } from '../dashboard.component';

@Component({
  selector: 'learning-analytics-cockpit-timeline',
  animations: [
    smoothWidth, smoothHeight],
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
})
export class TimelineComponent implements AfterViewInit, OnChanges {
  @Input() step!: ZoomSteps;
  @Input() study: IStudyData | undefined;
  @Input() selectedSemester: number | null = null;
  @Input() selectedCourse = '';
  @Input() isLecture = true;

  offSet = 12;

  constructor(private cdref: ChangeDetectorRef) {}

  // Solves expression change after check
  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  ngAfterViewInit(): void {
    this.setWidthAndPostiionOfSlider();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      this.setWidthAndPostiionOfSlider();
    }
  }

  getHeightOfSlider() {
    const slider = document.getElementById('slider');
    const timeline = document.getElementById('timeline_content');

    if (!slider || !timeline) {
      return;
    }

    const computedStyle = getComputedStyle(timeline);
    let timelineHeight = timeline.clientHeight;
    timelineHeight -=
      parseFloat(computedStyle.paddingTop) +
      parseFloat(computedStyle.paddingBottom);

    return timelineHeight + 'px';
  }

  setWidthAndPostiionOfSlider() {
    const slider = document.getElementById('slider');
    const timeline = document.getElementById('timeline_content');

    if (!slider || !timeline) {
      return;
    }

    slider.style.zIndex = '10';

    let timelineWidth: number;

    let semester: HTMLElement | null;
    let course: HTMLElement | null;
    let semesterBounding: DOMRect;
    let semesterWidth: number;
    let semesterLeft: number;

    let computedStyle: CSSStyleDeclaration;

    if (this.step === 'STUDY') {
      computedStyle = getComputedStyle(timeline);

      timelineWidth = timeline.clientWidth;

      slider.style.width = timelineWidth + 'px';
      slider.style.left = 0 + 'px';
    }

    // TODO refactor, share same code
    if (this.step === 'SEMESTER') {
      semester = document.getElementById('semester' + this.selectedSemester);

      if (!semester) {
        return;
      }

      semesterBounding = semester.getBoundingClientRect();
      computedStyle = getComputedStyle(semester);

      semesterWidth = semester.clientWidth + this.offSet;
      semesterWidth -=
        parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight);

      semesterLeft = semesterBounding.left;
      semesterLeft -=
        parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight) +
        this.offSet * 1.5;

      slider.style.left = `${semesterLeft}px`;
      slider.style.width = `${semesterWidth}px`;
    }

    if (this.step === 'COURSE') {
      course = document.getElementById(this.selectedCourse);

      if (!course) {
        return;
      }

      semesterBounding = course.getBoundingClientRect();
      computedStyle = getComputedStyle(course);

      semesterWidth = course.clientWidth + this.offSet;
      semesterWidth -=
        parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight);

      semesterLeft = semesterBounding.left;
      semesterLeft -=
        parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight) +
        this.offSet;

      slider.style.left = `${semesterLeft}px`;
      slider.style.width = `${semesterWidth}px`;
    }
  }
}

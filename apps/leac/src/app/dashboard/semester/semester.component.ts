import { state, trigger, style, transition, animate, query, animateChild } from '@angular/animations';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ISemesterData } from '@learning-analytics-cockpit/services';
import { smoothHeight } from '../../util/animation';
import { ZoomSteps } from '../dashboard.component';

@Component({
  selector: 'learning-analytics-cockpit-semester',
  animations: [smoothHeight,
  trigger('switchTimeslot',[
    state('big', style({
      width: '80%',
    })),
    state('small', style({
      width: '20%',
    })),
    transition('small <=> big', [
      animate('.5s ease')
    ])
  ]),
  trigger('appear', [
    state('in', style({
      'max-height': '100px', 'opacity': '1', 'visibility': 'visible'
  })),
  state('out', style({
      'max-height': '0px', 'opacity': '0', 'visibility': 'hidden'
  })),
    transition('out => in', [
      animate('.5s ease'),
    ]),
    transition('in => out', [
      animate('.5s ease')
    ]),
  ])],
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.scss'],
})
export class SemesterComponent implements OnChanges {
  @Input() step!: ZoomSteps;
  @Input() semester!: ISemesterData;
  @Input() selected!: boolean;
  @Input() selectedCourse = '';
  @Input() isLecture = true;

  @Output() semesterSelected = new EventEmitter<string>();
  @Output() isLectureSelected = new EventEmitter<boolean>();

  @Output() stepSelected = new EventEmitter<ZoomSteps>();
  @Output() courseSelected = new EventEmitter<string>();

  open = false;

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {

    if (this.step === 'SEMESTER') {
      if (changes['selected'] || changes['step']?.previousValue === 'COURSE') {
        setTimeout(() => this.open = this.selected, 300);
      }
      // this.open = this.selected;
    } else {
      this.open = false;
    }
  }

  selectCourse(courseId: string) {
    this.selectedCourse = courseId;
    this.selectSemester();
    this.courseSelected.emit(this.selectedCourse);
  }

  selectStep(step: ZoomSteps) {
    this.step = step;
    this.stepSelected.emit(step);
  }

  selectSemester() {
    this.semesterSelected.emit(this.semester.name);
  }

  toggleLecturePeriod(lecture: boolean | null = null) {
    if (lecture === null) {
      this.isLecture = !this.isLecture;
    } else {
      this.isLecture = lecture;
    }

    this.isLectureSelected.emit(this.isLecture);
  }
}

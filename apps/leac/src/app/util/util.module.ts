import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmoothHeightDirective } from './directives/smooth-height.directive';
import { SmoothWidthDirective } from './directives/smooth-width.directive';

@NgModule({
  declarations: [SmoothHeightDirective, SmoothWidthDirective],
  imports: [CommonModule],
  exports: [SmoothHeightDirective, SmoothWidthDirective],
})
export class UtilModule {}

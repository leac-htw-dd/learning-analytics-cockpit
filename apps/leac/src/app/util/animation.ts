import { animate, style, transition, trigger } from '@angular/animations';

export const smoothHeight = trigger('grow', [
  // transition('void <=> *', []),
  transition(
    'void <=> STUDY',
    [style({ height: '{{startHeight}}px', opacity: 0 }), animate('.7s ease')],
    {
      params: { startHeight: 0 },
    }
  ),
  transition(
    '* <=> *',
    [style({ height: '{{startHeight}}px', opacity: 0 }), animate('.5s ease')],
    {
      params: { startHeight: 0 },
    }
  ),
]);

export const smoothWidth = trigger('grow', [
  transition('void <=> *', []),
  transition(
    '* <=> *',
    [style({ width: '{{startWidth}}px', opacity: 0 }), animate('.5s ease')],
    {
      params: { startWidth: 0 },
    }
  ),
]);

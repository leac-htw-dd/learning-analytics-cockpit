import { Directive, OnChanges, Input, HostBinding, ElementRef } from '@angular/core';

    @Directive({
      // eslint-disable-next-line @angular-eslint/directive-selector
      selector: '[smoothWidth]',
      // eslint-disable-next-line @angular-eslint/no-host-metadata-property
      host: { '[style.display]': '"block"', '[style.overflow]': '"hidden"' }
    })
    export class SmoothWidthDirective implements OnChanges {
      @Input() smoothWidth!: any;
      pulse!: boolean;
      startWidth!: number;

      constructor(private element: ElementRef) {}

      @HostBinding('@grow')
      get grow() {
        return { value: this.pulse, params: { startWidth: this.startWidth } };
      }

      setstartWidth() {
        this.startWidth = this.element.nativeElement.width;
      }

      ngOnChanges() {
        this.setstartWidth();
        this.pulse = !this.pulse;
      }
    }
